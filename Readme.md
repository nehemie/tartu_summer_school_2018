This repository provides material as well as supplementary data for
the workshop **Practical Introduction to Spatial Humanities with R**
of the Summer School [Digital Methods in Humanities and Social
Sciences](https://digitalmethods.ut.ee/), August 21-25, 2018,
University of Tartu, Estonia
