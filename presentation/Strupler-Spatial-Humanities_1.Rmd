---
title: "</br> Spatial Humanities <br>"
subtitle: "</br> A practical introduction with R"
author: "Néhémie Strupler"
date: "2018"
output:
  xaringan::moon_reader:
    seal: true
    self_contained: true
    css: ["./css/default.css", "./css/my-themes.css"]
    yolo: false
    nature:
      highlightStyle: dracula
      highlightLines: true
      ratio: '4:3'
      countIncrementalSlides: true
---
layout: true

```{r r-setup, include=FALSE, echo = FALSE}
  options(htmltools.dir.version = FALSE)
  knitr::opts_chunk$set(cache = TRUE)
  knitr::opts_chunk$set(message = FALSE)
```

```{r load_refs, echo=FALSE, cache=FALSE}
  library(RefManageR)
  BibOptions(check.entries = FALSE,
            bib.style = "authoryear",
            cite.style = 'Chicago',
            style = "markdown",
            hyperlink = FALSE,
            dashed = FALSE)
  bib <- ReadBib("./biblio.bib", check = FALSE)
```

---
class: middle, center

## What are Spatial humanities?

.pull-left[
<img src="img/0-intro/magic_hat.jpg">

.my-caption[ Magic hat | CC0 ]
]

.pull-right[

<img src="img/0-intro/Aldrin_Apollo_11_wikipedia.jpg">

.my-caption[  Apollo 11 Mission | CC BY Wikipedia ]]

???
---
class: middle, center


<img src="img/0-intro/Eric_Gunther_Space_Chair_Wikipedia.jpg">

.bottom-left[.my-caption[  Space Chaire - CC BY Wikipedia  ]]

???
---
class: middle, center, inverse

<img src="data/world_pop/world_dens_wgs.jpg" style="float; width: 100%">

.bottom-left[.my-caption[World population density (WGS84), data: GPWv4, NASA]]

???
---
class: middle, center, inverse

<img src="data/world_pop/world_dens_azequal.jpg" style="float; width: 80%">

.bottom-left[.my-caption[World population density (Lambert azimuthal equal-area projection), data: GPWv4, NASA]]




---
class: middle, center, inverse

<img src="data/world_pop/world_dens_mercator.jpg" style="float; width: 90%">

.bottom-left[.my-caption[World population density (mercator), data: GPWv4, NASA]]





---
class: middle

## Spatial Humanities as a "discipline":

 1. Based on technologies of the digital humanities (data-mining, ...).
 2. Focused on geographic and concept of space
 3. Based on research from the humanities
 4. (Quantitatively-oriented)


???
---
class: middle

## Program of the day

> 1. Basics of spatial data and R
> 2. Hands-on (1)
> 3. Hands-on (2)





---
class: center, middle, inverse

## Excursus: Free Software and Free Science

![Two bits](./img/1-free_science/two-bits.jpg)

.my-caption[Christopher M. Kelty, **Two Bits: The Cultural
Significance of Free Software**, Duke University Press, 2008]

> "Free Software is a set of practices for the distributed
> collaborative creation of software source code that is then made
> openly and freely available through a clever, unconventional use of
> copyright law"
> `r Citet(bib, "Kelty2008twobits", after = ", 2", .opts = list(hyperlink = "to.bib"))`


???
---
class: middle

### Diversity makes the difference

.pull-left[
<img src="./img/1-free_science/PhDKnowledge_012.png">
]

.pull-right[

- Science is cumulative

- Different points of view stabilise knowledge

- Diversity invigorates problem solving

- Scientists are "hackers" too!
   ]

.bottom-left[.my-caption[
  Matt Might, [http://matt.might.net](http://matt.might.net/articles/phd-school-in-pictures/)
   CC BY-NC 2.5
    ]]



???
---
class: middle

### Functions of the scientific community

.pull-left[
- Inspiration

- Motivation

- Scrutiny
]

.pull-right[
<img src="./img/1-free_science/MacacaNigraSelfPortrait.jpg" style="float; width: 70%">

.my-caption[Self-portrait of a Macaca Nigra ("Monkey Selfie") |  CC0 ]
]


???
---
class: middle

.pull-left[
**Free Software and Science: some similarities**
  - Peer review
  - Validation and replication
  - Culture of credit, civility
  - Reputation
  - Communication

**Some dissimilarities**
 - How projects are developed, maintained, contested, and how ideas
   are exchanged
 - Challenge what is taken for granted in science
    ]

.pull-right[
<img src="./img/1-free_science/OpeningScience.png">

.my-caption[Bartling et al. 2014, Opening Science (Cover),
  https://doi.org/10.1007/978-3-319-00026-8  |  CC BY-NC 3.0]
]

???
---
class: middle


.pull-left[
<img src="./img/1-free_science/ResearchCulture.png">

.my-caption[ [Bartling et al. 2014, Opening Science (fig. 3,
  10)](http://doi.org/10.1007/978-3-319-00026-8_1)  |  CC BY-NC 3.0]
  ]


.pull-right[

**A key: Modifiability**

  - Centuries of adaptation to 'stable knowledge'

  - New paradigms with constant modulation
   ]


???
---
class: middle


### Challenge the ‘Power of Knowledge’

  - New IT render the knowledge more dynamic
  - New practices: ex. "Push and Publish" workflow
  - Reconcile process and result

.right-column[<img src="./img/1-free_science/DynamicPublicationFormats6.png" > ]

.bottom-left[.my-caption[ [Heller et al. 2014, Opening Science
  (fig. 5, 200)](http://dx.doi.org/10.1007/978-3-319-00026-8_13)  |
  CC BY-NC 3.0 ]]


???
---
class: middle

### Brings reproducibility in the focus


> "We often forget that scientific knowledge is reliable not because
scientists are more clever, objective or honest than other people, but
because their claims are exposed to criticism and replication."
`r Citet(bib, "Fanelli2013misconduct", .opts = list(hyperlink = "to.bib"))`


.right-column[<img src="./img/1-free_science/KieranHealy--workflow-wide.png">]

.bottom-left[.my-caption[Kieran Healy's Homepage http://kieranhealy.org/resources/ | Kieran Healy] ]


???
---
class: center, middle, inverse

### What to know more?

![](./img/1-free_science/yoda.png)

 Read the source, Luke!

.bottom-left[.my-caption[
[dannex10](https://piq.codeus.net/picture/65121/yoda) | CC BY 3.0 |
via [piq.codeus.net](https://piq.codeus.net/about)]]

---

## What does that mean?

1. Download this presentation:
   [gitlab.com/nehemie/tartu_summer_school_2018](https://gitlab.com/nehemie/tartu_summer_school_2018)

2. Take the survey!
   https://framaforms.org/evaluation-workshop-spatial-humanities-1531917082

<iframe
src="https://framaforms.org/evaluation-workshop-spatial-humanities-1531917082"
width="100%" height="400" border="0" ></iframe>


---
class: center, middle

# 1. Why R?

---
class: center


### Getting numbers and visualisation right is hard

<img src="img/2-r/why/C6mMjf5U8AEC794.jpg" style="float; width: 60%;
margin-left: 1%; margin-bottom: 0.5em;">

> \+ geom_nonsense()
>
> <a href="https://t.co/7s2sVnc1MM">pic.twitter.com/7s2sVnc1MM</a>
>
> Sean J. Taylor (@seanjtaylor),
> <a href="https://twitter.com/seanjtaylor/status/840357353631309824"> March 11, 2017</a>


.my-caption[via Tim Appelhans, Jena 2017]

--

> [That's how I like my pies, impossibly
> large.](https://twitter.com/bryancshepherd/status/840386408095313920)

???
---
class: center
count: true

## It's perfect for cooking data


--

<img src="img/2-r/why/ratatouille.jpg" style="float; width: 80%">
<!--![](img/ratatouille.jpg)-->

???
---
class: center
count: false

## It's perfect for cooking data

<img src="img/2-r/why/datatouille.jpg" style="float; width: 80%">
<!--![](img/datatouille.jpg)-->

.my-footnote[Adapted from the INRIA MOOC 'Introduction to a Web of Linked Data']





---

## And It's a great community !

 - blogs
 - \#rstats
 - Sharing materials: books, tutorials, workshop materials

### Material I used for the workshop

 - [Introduction to visualising spatial data in R (Robin Lovelace and
   James Cheshire)
   ](https://cran.r-project.org/doc/contrib/intro-spatial-rl.pdf)
 - [Introduction to mapping with {sf} & Co. (Sébastien Rochette)
   ](https://statnmap.com/2018-07-14-introduction-to-mapping-with-sf-and-co/)
 - [**Geocomputation with R, The basics** (Jannes Muenchow, Robin
   Lovelace)](https://github.com/jannes-m/erum18_geocompr)
 - ... many other





---
class: inverse, center, middle

# Crash course in spatial data

---
class: middle

## Geographic Information

- Everything is "geo-data" (almost)

- A **G**eographic **I**nformation **S**ystem is a system for the
  analysis, manipulation and visualisation of geographical data
  `r Cite(bib, "Longley2005geographic")`.

- It associates information (non-geographical data) with places
  (geographical data)

- It's a **tool** to describe, answer and predict (cluster?, pattern?
  relation?)


???
---
class: middle

## Geographic Data

- **x and y coordinates** +  **attributes**

>    58°23′N, 26°43′E, Tartu

- Simplified representation of the world

- Two data models for representing digitally geographic data:
  + **vector**
  + **raster**

???
---
## Geo data models

1. **Vector**

```{r anamed-vector, fig.width=7, fig.height=2, echo = FALSE}
par(mar = rep(0, 4))
plot.new()
text(0.4, 0.5, "ANAMED", cex = 8 )
```
2. **Raster**

```{r anamed, fig.width=7, fig.height=2, echo = FALSE}
library(raster)
par(mar = rep(0, 4))
logo <- raster::raster("./data/2017-03-25-Anamed_logo.tiff")
logo@data@names <- "name"
ras_logo <- logo
plot(ras_logo, axes = FALSE, legend=FALSE, col = c("black", "white"))
```

---

# Vector data model

.pull-left[
```{r vector-data-model, echo=FALSE, fig.width=5, fig.height=3}
library(sf)

p = st_point(c(0, 0.5))

pts = matrix(data = sample(seq(0.1, 1, 0.05), 6),
             ncol = 2,
             byrow = TRUE)
l = st_linestring(pts)

pts = matrix(data = sample(seq(0.1, 1, 0.05), 8)[c(1:6, 1:2)],
             ncol = 2, byrow = TRUE)
poly = st_polygon(list(pts))


par(mar = rep(0, 4))
plot(p, cex = 2, pch = 16, col = "blue",
     xlim = 0:1,
     ylim = 0:1)
  plot(l, add = TRUE, col = "darkred", lwd = 2)
  plot(poly, add = TRUE)
```
]

.pull-right[
```{r vector-data-model-bg, echo=FALSE, fig.width=5, fig.height=3}
par(mar = rep(0, 4))
plot(p, cex = 2, pch = 16, col = "red",
     xlim = 0:1,
     ylim = 0:1)
  plot(l, add = TRUE, col = "blue", lwd = 2)
  plot(poly, add = TRUE, col = "darkred")
```
]

Main features :
- "points", "lines" and "polygons" (+ rare others)
- Good for qualitative / discrete data
- (May be) Topological
- High level of abstraction (meaning change with context)

Example :
- Open Street Map
- Maps in general

???
---
layout: true
# Raster data model

---

```{r a, fig.width=12, fig.height=7, echo = FALSE}
layout(matrix(c(1:3), ncol = 3))
par(mar = rep(0, 4))
a <- extent(0,16,0,100)
ras_letter_a <- crop(logo, a)
plot(ras_letter_a, col = c("black", "white"), axes = FALSE,
     legend = F, bty = "n", box = FALSE)

ras_letter_a <- rasterToPolygons(ras_letter_a)
plot(ras_letter_a, col = c("black","white")[as.factor(ras_letter_a$name)],
     border = c("white","black")[as.factor(ras_letter_a$name)])
plot(ras_letter_a)
text(ras_letter_a, ras_letter_a$name, cex = 0.6, font = 2)
```




---

```{r a_in_grey, fig.width=12, fig.height=7, echo = FALSE}
layout(matrix(c(1:2), ncol = 2))
par(mar = rep(0, 4))
ras_letter_a[ras_letter_a$name != 255,] <- sample.int(15, length(ras_letter_a[ras_letter_a$name != 255,]), replace = TRUE)
plot(ras_letter_a, col = c(grey.colors(length(unique(ras_letter_a$name)),  start = 0, end = 0.5)[ras_letter_a$name]))
plot(ras_letter_a)
text(ras_letter_a, ras_letter_a$name, cex = 0.4, font = 2)
```
---

Features:
- Continuous rectangle of cells of same size
- Have a resolution
- Good for continuous data
- Are big

Example:
- aerial photography
- satellite-based remote sensing

???
---
layout: true

## Coordinate Reference Systems (CRS)

- required to locate geo-data






---

** Geographical **

.pull-left[
![](./img/vector_lonlat-geocompr.png)
]

.pull-right[
- longitude (E-W / Prime Meridian) + latitude (N-S / equator)
- angulare distance
- have a `datum` (ellipsoid + position center of the earth)
- few different models
]

.bottom-left[.caption[ [Lovelace, Nowosad, Muenchow, Geocomputation with R, Fig.2 .1](https://geocompr.robinlovelace.net/spatial-class.html#vector-data) ]]
---

**Projected**

.pull-left[
![](./img/vector_projected.png)
]

.pull-right[
- "Flatten" the 3D shape of the Earth onto a 2D plane
- linear distance (meter)
- based on a geographic CRS
- Distord at least on of direction, distance, and shape
- Especially suitable for "local" analysis
- a lot of different projections
]

.bottom-left[.caption[ [Lovelace, Nowosad, Muenchow, Geocomputation with R, Fig.2 .1](https://geocompr.robinlovelace.net/spatial-class.html#vector-data) ]]



???
---
layout: false






---
# References

```{r, 'refs', results='asis', echo=FALSE}
PrintBibliography(bib)  #, start = 1, end = 7)
```
